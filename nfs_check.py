#!/usr/bin/env python

"""
This script is able to detect hanging mounted nfs shares.

"""

__author__ = "Jochen Schlick"
__revision__ = "0.1"
__license__ = "GPL-3"

import os, re, sys

mountNumRawCount = 0
mountNumCount = 0

#Z######################################################################
def splitMountElements(mountProcContent):

  """
  splitting the content of the proc file in several NFS mount records
  """

  # used as mount record delimiter
  mountDel       = 'device '

  # use delimiter of records (blank line)
  #print("splitting..."),
  mountElements = re.split(mountDel, mountProcContent)

  # remove first garbage/empty element
  if len(mountElements) > 0:
    #print "garbage: \"%s\"" % mountElements[0]
    mountElements.pop(0)

  # remove leading and trailing LF in records
  newlineChars = "\n"
  for i in range(len(mountElements)):
    mountElements[i] = mountElements[i].lstrip(newlineChars)
    mountElements[i] = mountElements[i].rstrip(newlineChars)

    #print "%d ----- " % i
    #print mountElements[i]

  #print " done (%d)" % len(mountElements)
  return mountElements


#Z#################################################################
def initMountDataRecAsDict(server, localmnt, rawRec):
  """
  create a mount record dict - so that all tag names exist
  """

  myRecAsDict = { 'SERVER'    : server,
                  'LOCALMNT'  : localmnt,
                  'RAWREC'    : rawRec}
  return myRecAsDict



#Z######################################################################
def processMounts(mountRecData, mountProcContent):
  """
  processing the concerning mount of the procfile content
  """
  global mountNumRawCount
  global mountNumCount

  rawMountElements = splitMountElements(mountProcContent)
  #print "processMounts: %s" % str(rawMountElements)

  # fill data structures
  for (i, rec) in enumerate(rawMountElements):

    #print "processMounts: %d   %s" % (i, str(rec))

    # get nfs mount
    # output line: "scp-nas1.comsoft.de:/srv/lv_usr mounted on /home1/usr with fstype nfs statvers=1.1"
    recordMatch       = re.search(r"(.*) mounted on (.*) with fstype nfs statvers=", rec)
    mountNumRawCount  = i+1

    if recordMatch != None:
      se            = recordMatch.group(1)
      lm            = recordMatch.group(2)

      if lm in mountRecData:
        # local mount already exist !! no new entry...
        print "DUP:mountRecData[key=%s] ignored: %s" % (str(i), str(mountRecData[lm]))

      else:
        # local mount not exist
        mountNumCount = mountNumCount + 1
        myRecAsDict = initMountDataRecAsDict(se, lm, rec)
        if myRecAsDict:
          mountRecData[lm] = myRecAsDict;
          #print "NEW:mountRecData[key=%s]: %s" % (str(i), str(mountRecData[lm]))

    #else:
      #print "processMounts: No NFS mount found in mount records (%d)" % i


  if (mountNumCount == 0):
    print "processMounts: No NFS mount found"
    sys.exit(1)


#Z######################################################################
def detectHang(mountRecData, mountLocal, oldHistList):
  """
  detecting the hanging NFS
  """

  #print "detectHang: %s" % str(mountRecData)

  # fill data structures
  error = ""
  acnt = 0
  wcnt = 0
  if mountLocal in mountRecData.keys():

    raw = mountRecData[mountLocal]['RAWREC']
    #print "detectHang: %s" % str(raw)

    # interesting values: ACCESS and WRITE position 3
    # output line "ACCESS: 1890 1908 0 256264 226800 34 2191 2407"
    # output line "WRITE: 485 485 0 23305528 65960 30 2273 2359"
    for line in re.split("\n", raw):
      #print "detectHang(line):%s" % line
      recordMatch   = re.search('(ACCESS|WRITE): [0-9]+ [0-9]+ ([0-9]+) ', line)

      if recordMatch != None:
        aw          = recordMatch.group(1)
        cnt         = int(recordMatch.group(2))

        #print "%s cnt=%d" % (aw, cnt)

        if aw == 'ACCESS':
          acnt      = cnt
          if (acnt != int(oldHistList[0])):
            error = "HANG (NFS ACCESS) #%d (old: %d)" % (acnt, oldHistList[0])
        else:
          wcnt      = cnt
          if (wcnt != int(oldHistList[1])):
            error = "HANG (NFS WRITE) #%d (old: %d)" % (wcnt, oldHistList[1])

    if len(error) > 0:
      print error
    else:
      print "OK"
    return [acnt, wcnt]

  else:
    print "detectHang: No NFS mount found"
    sys.exit(1)



#Z######################################################################
def writeHistoryElements(newHistList, historyFileName):
  """
  write the access cnt and write cnt to the history file
  """
  historyFile = open(historyFileName, 'w')
  historyFile.write(str(newHistList[0]))
  historyFile.write(" ")
  historyFile.write(str(newHistList[1]))
  historyFile.close()
  return


#Z######################################################################
def readHistoryElements(historyFileName):
  """
  read the access cnt and write cnt from the history file
  """

  try:
    historyFile = open(historyFileName, 'r')
  except IOError:
    print "readHistoryElements: history file not accessible"
    return [0, 0]

  historyContent = historyFile.read()
  historyFile.close()

  cntList = []
  #print "readHistoryElements: %s" % historyContent
  for counters in re.split(" ", historyContent):
    cntList.append(int(counters))

  if len(cntList) == 2:
    #print "readHistoryElements: %s" % str(cntList)
    return cntList
  else:
    print "readHistoryElements: bad history -> ignored"
    return [0, 0]



#Z########################################################################
def main():
  """
  nfs_check.py main program
  """

  global mountNumRawCount
  global mountNumCount

  # command line args
  mountProc  = "/proc/self/mountstats"

  if (len(sys.argv) < 3) or (len(sys.argv) > 4):
    print "\nUsage: nfs_check.py <local mount> <history-file> [proc-file]\n\n       proc-file=%s\n" % mountProc
    print "\n\n  the history-file contains two values:\n  access error count\n  write error count"
    sys.exit(1)

  mountLocal   = sys.argv[1]
  mountHistory = sys.argv[2]
  if len(sys.argv) == 4 :
    mountProc= sys.argv[3]

  #print "  mountLocal   = %s" % mountLocal
  #print "  mountHistory = %s" % mountHistory
  #print "  mountProc    = %s" % mountProc

  # read proc file
  mountProcFile = open(mountProc, 'r')
  mountProcContent = mountProcFile.read()
  mountProcFile.close()

  # get history data
  oldHistList = readHistoryElements(mountHistory)

  # start processing:
  mountRecordData = {}
  #print "Processing ProcFile: %s, %s" % (mountProc, mountLocal)
  processMounts(mountRecordData, mountProcContent)
  #print "  raw.Mount record count = %d" % mountNumRawCount
  #print "  res.Mount record count = %d" % mountNumCount

  # detect hanging NFS
  newHistList = detectHang(mountRecordData, mountLocal, oldHistList)

  # write history
  #print("writing history file..."),
  writeHistoryElements(newHistList, mountHistory)
  #print " done"


if __name__ == '__main__':

  main()
