# nfs_check

Linux python script which checks the status of nfs without accessing the concerning directories to detect hanging nfs shares. Instead it accesses /proc/self/mountstats.


Usage:

    nfs_check.py localMount historyFile [procFile]
    
    default procFile=/proc/self/mountstats

Example:

    nfs_check.py /home1/nfsshare /run/nfshistory_nfsshare
